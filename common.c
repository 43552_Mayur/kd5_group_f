#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "library.h"

// user function
void user_accept(user_t *u)
{
   // printf("User id  :");
   // scanf("%d",&u->id);
   u->id = get_next_user_id();
    printf("User name    :");
    scanf("%s",u->name);
    printf("User email   :");
    scanf("%s",u->email);
    printf("user phone no   :");
    scanf("%s",u->phone);
    printf("user password    :");
    scanf("%s",u->password);
    strcpy(u->role, ROLE_MEMBER);
}
void user_display(user_t *u){
printf("id=%d  Name=%s  email=%s  phone=%s %s=role \n",u->id,u->name,u->email,u->phone,u->role);
}
// book functions
void book_accept(book_t *b){
    // printf("Book id  :");
    // scanf("%d",&b->id);
    printf("Book name    :");
    scanf("%s",b->name);
    printf("Book Author   :");
    scanf("%s",b->author);
   
    printf("Book price   :");
    scanf("%lf",&b->price);
    printf("Book isbn    :");
    scanf("%s",b->isbn);

}
void book_display(book_t *b)
{
    printf("Book id = %d, Name = %s, Author = %s, price = %.2lf, isbn = %s \n",b->id,b->name,b->author,b->price,b->isbn);
}
// bookcopy 
void bookcopy_accept(bookcopy_t *c) {
    //printf("id: ");
    //scanf("%d",&c->id);
    printf("book id: ");
    scanf("%d",&c->bookid);
    printf("rack: ");
    scanf("%d",&c->rack);
    strcpy(c->status, STATUS_AVAIL);
}

void bookcopy_display(bookcopy_t *c){
    printf("Bookcopy id: %d,Book id: %d,Rack no: %d,Status: %s\n", c->id, c->bookid, c->rack, c->status);    
}

//issuerecord 
void issuerecord_accept(issuerecord_t *r) {
    //printf("id: ");
    //scanf("%d",&r->id);
    printf("copyid: ");
    scanf("%d",&r->copyid);
    printf("member id: ");
    scanf("%d",&r->memberid);
    printf("issue ");
    date_accept(&r->issue_date);
    //r->issue_date = date_current();
    r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
    memset(&r->return_date, 0, sizeof(date_t));
    r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) {
    printf("issue record: %d, copy: %d, member: %d, fine: %.2lf\n",r->id, r->copyid, r->memberid, r->fine_amount);
        printf("issue ");
        date_print(&r->issue_date);
        printf("return due ");
        date_print(&r->return_duedate);
        printf("return ");
        date_print(&r->return_date);
}
//payment functions
void payment_accept(payment_t *p){
   // printf("id: ");
   // scanf("%d",&p->id);
    printf("member id: ");
    scanf("%d",&p->memberid);
   // printf("type (fees/fine): ");
   // scanf("%s", p->type);
    strcpy(p->type, PAY_TYPE_FEES);
    printf("amount: ");
    scanf("%lf", &p->amount);
    p->tx_time = date_current();
    //if(strcmp(p->type, PAY_TYPE_FEES)==0)
           p->next_pay_duedate = date_add(p->tx_time,MEMBERSHIP_MONTH_DAYS);
    //  else
    //       memset(&p->next_pay_duedate, 0,sizeof(date_t));
}

void payment_display(payment_t *p) {
    printf("payment: %d, member: %d, %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
    printf("payment ");
    date_print(&p->tx_time);
    printf("payment due");
    date_print(&p->next_pay_duedate);
}

void user_add(user_t *u)
{
    FILE *fp;
    fp = fopen(USER_DB, "ab");
    if(fp == NULL) {
        perror("failed to open users file");
        return ;
    }
    fwrite(u,sizeof(user_t),1,fp);
    printf("user added into file.\n");
    fclose(fp);
}

void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;
	// open the file for reading the data
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}
	// read all books one by one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// if book name is matching partially, found 1
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	// close file
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}


int user_find_by_email(user_t *u, char email[])
{
    FILE *fp;
    int found = 0;
    fp = fopen(USER_DB, "rb");
    if(fp == NULL)
     {
        perror("failed to open users file");
        return found;
     }
     while(fread(u, sizeof(user_t), 1, fp) > 0){
         if (strcmp(u->email, email)==0) {
             found = 1;
             break;
         }
     }
     //file close
     fclose(fp);
    //return
     return found;
}

int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;
	// open the file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	// open the file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;
	// open the file
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

void change_password_by_id() {
	char password[30];
	int found = 0;
	int id;
	FILE *fp;
    user_t p;
	// open the file for reading the data
	printf("Enter user id :");
	scanf("%d",&id);
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&p, sizeof(user_t), 1, fp) > 0) {
		
		if(id == p.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new book details from user
		long size = sizeof(user_t);
		printf("Enter new password :");
		scanf("%s",p.password);

		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&p, size, 1, fp);
		printf("password updated.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("email not found.\n");
	// close books file
	fclose(fp);
}

void edit_profile_by_id() {

	int found = 0;
	int id;
	FILE *fp;
    user_t m;
	// open the file for reading the data
	printf("Enter user id :");
	scanf("%d",&id);
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open file");
		exit(1);
	}
	// read books one by one and check if book with given id is found.
	while(fread(&m, sizeof(user_t), 1, fp) > 0) {
		
		if(id == m.id) {
			found = 1;
			break;
		}
	}
	// if found
	if(found) {
		// input new book details from user
		long size = sizeof(user_t);
		printf("Enter new Name  :");
		scanf("%s",m.name);

		printf("Enter new phone number :");
		scanf("%s", m.phone);

		// take file position one record behind.
		fseek(fp, -size, SEEK_CUR);
		// overwrite book details into the file
		fwrite(&m, size, 1, fp);
		printf("profile updated successfully.\n");
	}
	else // if not found
		// show message to user that book not found.
		printf("not found.\n");
	// close books file
	fclose(fp);
}

int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	// open the file
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;
	// open the file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// change file pos to the last record
	fseek(fp, -size, SEEK_END);
	// read the record from the file
	if(fread(&u, size, 1, fp) > 0)
		// if read is successful, get max (its) id
		max = u.id;
	// close the file
	fclose(fp);
	// return max + 1
	return max + 1;
}

