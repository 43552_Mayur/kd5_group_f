#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "library.h"
#include "date.h"

void date_tester() {
	date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
	date_t d = {1, 1, 2000};
	date_t r = date_add(d, 366);
	date_print(&r);
	int diff = date_cmp(d1, d2);
	printf("date diff: %d\n", diff);
}

void tester(){
    user_t u;
    book_t b;
    user_accept(&u);
    user_display(&u);
    book_accept(&b);
    book_display(&b);
}

void sign_in() 
{
    char email[30],password[10];
    int invalid_user=0;
    user_t u;
    //input email & password from user
    printf("email:");
    scanf("%s",email);
    printf("password:");
    scanf("%s",password);
    //find user in the user file by email
    if (user_find_by_email(&u,email)==1)
    {
        //check input
       if(strcmp(password,u.password)==0)
        {
            //special case
            if(strcmp(email,EMAIL_OWNER)==0)
                strcpy(u.role,ROLE_OWNER);
             //if correct, call user_area on its role
              if(strcmp(u.role,ROLE_OWNER)==0)
                 owner_area(&u);
              else if(strcmp(u.role,ROLE_LIBRARIAN)==0)
                 librarian_area(&u);
              else if(strcmp(u.role,ROLE_MEMBER)==0)
                 member_area(&u);
              else
                 invalid_user = 1;
        }
         else
           invalid_user = 1;
   }
   else
     invalid_user = 1;
      
       if(invalid_user)
         printf("Invalid email,password or role.\n");
}
   

void sign_up() {
    // input user
    user_t u;
    user_accept(&u);
    // write user details
    user_add(&u);
    
     int res = get_next_user_id();
     res--;
     printf("\nYour Newly Generated User id =: %d ",res); 

}

int main( void )
{
    //date_tester();
    // tester();
    int choice;
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);

    return 0;
}