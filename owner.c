#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"library.h"

void owner_area(user_t *u){
    int choice;
    do{
      printf("\n\n0.Sign out\n1. Appoint Librarian\n2. Edit profile\n3. Change password\n4. book fine report\n5. book availabilty\n6.Bookcopies Report\n7.Books Reort\n Enter choice : ");
      scanf("%d",&choice);
      switch(choice)
      {
        case 1:
            appoint_librarian();
            break;
        case 2:// edit profile
			edit_profile_by_id();
            break;
        case 3:
            change_password_by_id();
            break;
        case 4:
            break;
        case 5: //cheak avalability
			bookcopy_checkavail_details();
            break;
        case 6: // bookcpies report
             display_copies_report();
            break;
        case 7: // BOOKS REPORT
             display_books_report();
            break;
        }
     }while(choice!=0);
}
void appoint_librarian() {
	// input librarian details
	user_t u;
	user_accept(&u);
	// change user role to librarian
	strcpy(u.role, ROLE_LIBRARIAN);
	// add librarian into the users file
	user_add(&u);
}

void display_copies_report(){
	FILE *fp;
    bookcopy_t u;
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
	{
		perror("Cannot open file \n");
		exit(0);
	}
	// read contents from folder
	while(fread(&u, sizeof(bookcopy_t), 1, fp) > 0)
	 {
		bookcopy_display(&u);
	 }
	fclose(fp);
}

void display_books_report(){
	FILE *fp;
    book_t u;
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
	{
		perror("Cannot open file \n");
		exit(0);
	}
	// read contents from folder
	while(fread(&u, sizeof(book_t), 1, fp) > 0)
	 {
		book_display(&u);
	 }
	fclose(fp);
}
